package tema5;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class MonitoredData {
	private String startTime, endTime, activity;
	private String day;
	public MonitoredData(String startTime, String endTime, String activity) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
		setDay();
	}
	public String toString(){
		return "\nData[ activity: "+activity+", started:"+startTime+" , ended:"+ endTime+" ]";
	}
	private void setDay(){
		String dif[] = startTime.split("//s+");
		day = dif[0].trim();
	}
	
	public String getDay(){
		return day;
	}
	public String getActivity() {
		return activity;
	}
	public String getStartTime() {
		return startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public long getHours(){
		//2011-12-10 13:04:03
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime to = LocalDateTime.parse(endTime, formatter);
		LocalDateTime from = LocalDateTime.parse(startTime, formatter);
		LocalDateTime tempDateTime = LocalDateTime.from( from );

		long hours = tempDateTime.until( to, ChronoUnit.HOURS);
		long minutes = tempDateTime.until( to, ChronoUnit.MINUTES);
		long seconds = tempDateTime.until( to, ChronoUnit.SECONDS);
		return hours+minutes/60+seconds/360;
	}
	public long getMinutes(){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime to = LocalDateTime.parse(endTime, formatter);
		LocalDateTime from = LocalDateTime.parse(startTime, formatter);
		LocalDateTime tempDateTime = LocalDateTime.from( from );

		long hours = tempDateTime.until( to, ChronoUnit.HOURS);
		long minutes = tempDateTime.until( to, ChronoUnit.MINUTES);
		long seconds = tempDateTime.until( to, ChronoUnit.SECONDS);
		return hours*60+minutes+seconds/60;
	}
}


