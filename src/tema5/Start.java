package tema5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Start {
	public static void main(String[] args){
		FileHandler f = new FileHandler();
		List<MonitoredData> activities = new ArrayList<MonitoredData>();
		activities = f.parseActivities(f.readFile());
		System.out.println(activities);
		
		//1
		Map<String,Integer> days = f.ex1();
		
		//2
		Map<String,Integer> activityTypes = f.ex2(activities);
	    
	    //3
	    Map<Integer,Map<String,Integer>> dailyActivities =f.ex3();
	    
	    //4
	    f.ex4();
	    f.ex5();
	}
}

