package tema5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class FileHandler {

	List<String> list = new ArrayList<String>();
	
	Map<String,Integer> days = new HashMap<String,Integer>();//folosim hashmap ptr memorarea zielelor ptr o cautare mai eficienta Integer- dummy values
	
	List<MonitoredData> activities = new ArrayList<MonitoredData>();

	Map<String,Integer> activityTypes = new HashMap<String, Integer>();
	List<String> keys = new ArrayList<String>();//we create a list of keys to iterate easier trough the map
	
	Map<Integer, Map<String, Integer>> dailyActivities = new HashMap<Integer, Map<String, Integer>>();
	List<Integer> keys2 = new ArrayList<Integer>();
	
	Map<String, Long> activitylength = new HashMap<String, Long>();
	List<String> keys3;
	
	List<String> shorties = new ArrayList<String>();
	Map<String, Long> mapShorties;
	public List<String> readFile(){
		String fileName="C:/Users/Diana/Desktop/Java/tema5/src/activities.txt";
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			list = stream.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}
	public List<MonitoredData> parseActivities(List<String> list){
		for(String line: list){
			String elements[];
			elements=line.split("\\s{2,100}");
			activities.add(new MonitoredData(elements[0].trim(), elements[1].trim(), elements[2].trim()));
		}
		return activities;
	}
	public Map<String,Integer> ex1(){
		int i=0;
		List<String> l = activities.stream()
				.map(d -> d.getDay())
				.collect(Collectors.toList());
		System.out.println(l.size());
		for(MonitoredData d: activities){
			if(!days.containsKey(d))
				days.put(d.getDay(), i); //i represents day number
				i++;
		}
		System.out.println(days.size()+"different days");
		return days;
	}
	public Map<String, Integer> ex2(List<MonitoredData> activities){
		//activities.stream()
		final AtomicInteger poz = new AtomicInteger(0);
		int arr[] = new int[activities.size()];
		
		for(MonitoredData d: activities){
			if(!activityTypes.containsKey(d.getActivity())){
				activityTypes.put(d.getActivity(), 0);
				keys.add(d.getActivity());
			}
			else{
				activityTypes.put(d.getActivity(), Integer.valueOf(activityTypes.get(d.getActivity()) + 1));
			}
			writeResult2();
		}
	    return activityTypes;
	}
	public void writeResult2(){
		FileWriter fstream;
	    BufferedWriter out;
	    try {
			fstream = new FileWriter("ex2.txt");
		    out = new BufferedWriter(fstream);
		    for(String key: keys){
		    	out.write("K:"+key+" V:"+activityTypes.get(key));
		    	out.newLine();
		    }
		    out.close();
		} catch (IOException e) {
			System.out.println("ERROR!!! couldn not write in file");
		}
	}
	
	public Map<Integer, Map<String, Integer>> ex3() {
		for(MonitoredData d: activities){
			if(!dailyActivities.containsKey(days.get(d.getDay()))){
				List<MonitoredData> today = new ArrayList<MonitoredData>();
				today.add(d);
				dailyActivities.put(days.get(d.getDay()), ex2(today));
				keys2.add(days.get(d.getDay()));
			}
		}
		writeResult3();
		return dailyActivities;
	}
	public void writeResult3(){
		FileWriter fstream;
	    BufferedWriter out;
	    try {
			fstream = new FileWriter("ex3.txt");
		    out = new BufferedWriter(fstream);
		    for(Integer key: keys2){
		    	out.write("K:"+key+" V:"+dailyActivities.get(key));
		    	out.newLine();
		    }
		    out.close();
		} catch (IOException e) {
			System.out.println("ERROR!!! couldn not write in file");
		}
	}
	public void ex4(){
		activitylength = activities.stream()
				.filter(d->d.getHours()>10)
				.collect(Collectors.groupingBy
				(MonitoredData::getActivity,Collectors.summingLong(d->d.getHours())));
		System.out.println(activitylength.size());
		writeResult4();
	}
	
	public void writeResult4(){
		FileWriter fstream;
	    BufferedWriter out;
	    try {
			fstream = new FileWriter("ex4.txt");
		    out = new BufferedWriter(fstream);
		    out.write("ksize: "+keys.size());
		    for(String key: keys){
		    	out.write("K:"+key+" V:"+activitylength.get(key));
		    	out.newLine();
		    }
		    out.close();
		} catch (IOException e) {
			System.out.println("ERROR!!! couldn not write in file");
		}
	}
	
	public void ex5(){
		mapShorties = activities.stream()
				.filter(d-> d.getMinutes()<5)
				.collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingLong(MonitoredData::getMinutes)));
		mapShorties.forEach((k,v)->{
			//System.out.println(v*100+"/"+activityTypes.get(k)+"="+v*100/activityTypes.get(k));
			if((v*100)/activityTypes.get(k)>9){
				shorties.add(k);
			}
		});
		System.out.println(shorties);
	}
}
